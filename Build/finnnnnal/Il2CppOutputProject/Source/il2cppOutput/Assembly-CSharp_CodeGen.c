﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DisableObjectAfterDelay::Awake()
extern void DisableObjectAfterDelay_Awake_m257142B35642921833E778F36587AA3192378465 (void);
// 0x00000002 System.Collections.IEnumerator DisableObjectAfterDelay::DisableAfterDelay()
extern void DisableObjectAfterDelay_DisableAfterDelay_m5B9F12CEF60C3495D2A1465F800C269A35AF7E2E (void);
// 0x00000003 System.Void DisableObjectAfterDelay::.ctor()
extern void DisableObjectAfterDelay__ctor_m276DE3357BEE86BC1C3FC21D023F0BFCF150E127 (void);
// 0x00000004 System.Void DisableObjectAfterDelay/<DisableAfterDelay>d__4::.ctor(System.Int32)
extern void U3CDisableAfterDelayU3Ed__4__ctor_m2C91CE34B94743AF7EF50E9214C9AEA7FCA79515 (void);
// 0x00000005 System.Void DisableObjectAfterDelay/<DisableAfterDelay>d__4::System.IDisposable.Dispose()
extern void U3CDisableAfterDelayU3Ed__4_System_IDisposable_Dispose_mA39E91D4A0D4AFFEAA7E41B928EC362268D50DD1 (void);
// 0x00000006 System.Boolean DisableObjectAfterDelay/<DisableAfterDelay>d__4::MoveNext()
extern void U3CDisableAfterDelayU3Ed__4_MoveNext_m8C18702750633BB81526459A569616D16E4910C6 (void);
// 0x00000007 System.Object DisableObjectAfterDelay/<DisableAfterDelay>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisableAfterDelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25813633BAE9E500DB07C70EC0EE5DDF835F8CBD (void);
// 0x00000008 System.Void DisableObjectAfterDelay/<DisableAfterDelay>d__4::System.Collections.IEnumerator.Reset()
extern void U3CDisableAfterDelayU3Ed__4_System_Collections_IEnumerator_Reset_m2B139A7C8C9D16F95C1B3885A166600AE585A184 (void);
// 0x00000009 System.Object DisableObjectAfterDelay/<DisableAfterDelay>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CDisableAfterDelayU3Ed__4_System_Collections_IEnumerator_get_Current_m0476275292D9774988C3B9B7F9C57B5082CBCE81 (void);
// 0x0000000A System.Void MenuManager::TrainingMode()
extern void MenuManager_TrainingMode_m40DC838AC258FBDA58D6CDE823DA957DFA3EC017 (void);
// 0x0000000B System.Void MenuManager::TutorialMode()
extern void MenuManager_TutorialMode_mA48FDDA254090470A8393FB52C56FED712D07CD9 (void);
// 0x0000000C System.Void MenuManager::OperationMode()
extern void MenuManager_OperationMode_mF6567B128251132E3BC86119AAD5952A7A9095BD (void);
// 0x0000000D System.Void MenuManager::.ctor()
extern void MenuManager__ctor_m07A22DFDD90E3164393F8BDE06DAEF5AFA786CF2 (void);
// 0x0000000E System.Void OrganButtonManager::ShowIliacSeperatlyButtonDown()
extern void OrganButtonManager_ShowIliacSeperatlyButtonDown_mE86B8D0ABF2645713A2314B514C0E92AAC0CAB95 (void);
// 0x0000000F System.Void OrganButtonManager::ShowFemurSeperatlyButtonDown()
extern void OrganButtonManager_ShowFemurSeperatlyButtonDown_m9E981F2769090F70833793E0847712DDB0179EDE (void);
// 0x00000010 System.Void OrganButtonManager::LoadMeanMenu()
extern void OrganButtonManager_LoadMeanMenu_mC7D6BD63CCE37B50705F6FAFDE453DFA967F324A (void);
// 0x00000011 System.Void OrganButtonManager::ShowIliacButtonDown()
extern void OrganButtonManager_ShowIliacButtonDown_m3BDEA4D8230A2A359B5DA0DB348AD74603226D66 (void);
// 0x00000012 System.Void OrganButtonManager::ShowFemurButtonDown()
extern void OrganButtonManager_ShowFemurButtonDown_mBB95E8D881FDA5DF48006C84FAFB93690755203F (void);
// 0x00000013 System.Void OrganButtonManager::ShowIliac()
extern void OrganButtonManager_ShowIliac_mD01D3F5A08B38CD571DA931864A723C94B44FD3A (void);
// 0x00000014 System.Void OrganButtonManager::ShowFemur()
extern void OrganButtonManager_ShowFemur_m56CF9FC75C79C466937C2A475E2232DF4D60CCCD (void);
// 0x00000015 System.Void OrganButtonManager::Back()
extern void OrganButtonManager_Back_m3463414A603340BDD3EBE6B8C32CB51D10F69625 (void);
// 0x00000016 System.Void OrganButtonManager::.ctor()
extern void OrganButtonManager__ctor_mA4AF11F58F85BE830FC0252F885C9C660EE2820E (void);
// 0x00000017 System.Void TutoText::Start()
extern void TutoText_Start_m45D41A03F949D48A0BD4DC2B60B4422A7947294E (void);
// 0x00000018 System.Collections.IEnumerator TutoText::changeText()
extern void TutoText_changeText_m0A4ABE4A295BE6F936EB3D1D0FC9DD2250E9F01B (void);
// 0x00000019 System.Void TutoText::.ctor()
extern void TutoText__ctor_m412B2AA162971446A7F8AFB6AE4A796D4D5BF19D (void);
// 0x0000001A System.Void TutoText/<changeText>d__4::.ctor(System.Int32)
extern void U3CchangeTextU3Ed__4__ctor_mB330C1623ECE7F111B177AE55030DF1D77681463 (void);
// 0x0000001B System.Void TutoText/<changeText>d__4::System.IDisposable.Dispose()
extern void U3CchangeTextU3Ed__4_System_IDisposable_Dispose_m5A7DFD8A15AF50E78CC0E93E96A1E6693477C625 (void);
// 0x0000001C System.Boolean TutoText/<changeText>d__4::MoveNext()
extern void U3CchangeTextU3Ed__4_MoveNext_m611360DE62ECB9BB15198A4D7D684911FFA4AE8F (void);
// 0x0000001D System.Object TutoText/<changeText>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CchangeTextU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB1F7B9FE3E8378FDB8C74A19C96901C075BB839 (void);
// 0x0000001E System.Void TutoText/<changeText>d__4::System.Collections.IEnumerator.Reset()
extern void U3CchangeTextU3Ed__4_System_Collections_IEnumerator_Reset_mFCD0DF78872A7A730C40E2B26578444106376932 (void);
// 0x0000001F System.Object TutoText/<changeText>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CchangeTextU3Ed__4_System_Collections_IEnumerator_get_Current_mCCB3E244B009BBC0D66F9A3483B50D48756BDDC5 (void);
// 0x00000020 System.Void UIManagerTrainingMode::Awake()
extern void UIManagerTrainingMode_Awake_m71DC8B18A515BC08A1940A448BB5719F108E65AB (void);
// 0x00000021 System.Void UIManagerTrainingMode::Update()
extern void UIManagerTrainingMode_Update_m320C0460DA1DCB187C3761CF3425F2B36ECC26BB (void);
// 0x00000022 System.Void UIManagerTrainingMode::RestartTuto()
extern void UIManagerTrainingMode_RestartTuto_m94F9FB57EEA717669862B7A9F5C007E6CB8FB2EF (void);
// 0x00000023 System.Void UIManagerTrainingMode::MainMenu()
extern void UIManagerTrainingMode_MainMenu_mA0435FE7D0084E345EB05E2D1CE3F07C1DC18E06 (void);
// 0x00000024 System.Void UIManagerTrainingMode::.ctor()
extern void UIManagerTrainingMode__ctor_mAB12FDB875F94B67FAE267516B441339E35F9E9C (void);
// 0x00000025 System.Void controller::Start()
extern void controller_Start_mB26A4CABF956423246728470990213D287A89728 (void);
// 0x00000026 System.Void controller::Update()
extern void controller_Update_mDC1CCBA134CA67C1ABF0FD50B477BE2F339A6E8D (void);
// 0x00000027 System.Void controller::.ctor()
extern void controller__ctor_mCC811B140CF89AADD1C0FA58475114264BBF8379 (void);
// 0x00000028 System.Void cutPaint::Start()
extern void cutPaint_Start_m0E8F15BFEEDA8B963D865768A5BB425C579AF7AE (void);
// 0x00000029 System.Void cutPaint::Update()
extern void cutPaint_Update_m35CDDD6CF29225D962C0CC62DA76A23FC9733571 (void);
// 0x0000002A System.Void cutPaint::.ctor()
extern void cutPaint__ctor_m7DB70F72753ADA43A7389F1AFA7147385FB5EF67 (void);
// 0x0000002B System.Void imageTrack::Start()
extern void imageTrack_Start_mD6AFC5AB0198D054A984660C9EB04F3BE52CBEDB (void);
// 0x0000002C System.Void imageTrack::Update()
extern void imageTrack_Update_m7D39085929333C0865293CF97C336CAC8E4198FE (void);
// 0x0000002D System.Void imageTrack::.ctor()
extern void imageTrack__ctor_m077A4E089D7B3B498C8E0C0FC850E452077578F4 (void);
// 0x0000002E System.Void NewCutPaint::Start()
extern void NewCutPaint_Start_m3432265C417B3002CED3696966E26383F90844C1 (void);
// 0x0000002F System.Void NewCutPaint::Update()
extern void NewCutPaint_Update_mAD2308D19F74958943525D1BEC6EBE008C8FD649 (void);
// 0x00000030 System.Void NewCutPaint::.ctor()
extern void NewCutPaint__ctor_m50326C6450CA38648BFF12E56DF163766A62D458 (void);
// 0x00000031 System.Void QRCodeManager::ActivateQR()
extern void QRCodeManager_ActivateQR_m785C3E1B4D782DE9336928669D130F2BEAA6D98F (void);
// 0x00000032 System.Void QRCodeManager::DisactivateQR()
extern void QRCodeManager_DisactivateQR_mDC70CACA14E2D7F1F3F02388298FAA08BFA2E04F (void);
// 0x00000033 System.Void QRCodeManager::.ctor()
extern void QRCodeManager__ctor_m098D357199051BDF3C730F2221292CFE399AF4C1 (void);
// 0x00000034 System.Void SpeechComand::Update()
extern void SpeechComand_Update_mFB38D6541D44393E55F84E3ECDA58EF0A23990FD (void);
// 0x00000035 System.Void SpeechComand::SetPosition()
extern void SpeechComand_SetPosition_mA1DBC7BD199ADB006070B1C90D77573A2D0FB670 (void);
// 0x00000036 System.Void SpeechComand::Show(UnityEngine.GameObject)
extern void SpeechComand_Show_m82009FD51C38D6262E57D4C853E401F23F97239A (void);
// 0x00000037 System.Void SpeechComand::Zoom()
extern void SpeechComand_Zoom_m2CACFE71B1BBCCEDEF01841485CAEFF48000BE08 (void);
// 0x00000038 System.Void SpeechComand::Reset()
extern void SpeechComand_Reset_mEE9A8603DA5B1D881594FCB3998CD222370F6695 (void);
// 0x00000039 System.Void SpeechComand::Destroy(System.String)
extern void SpeechComand_Destroy_mE2A3F0BA7671C0D24EC818742E84B2E198389A73 (void);
// 0x0000003A System.Void SpeechComand::Delete(System.String)
extern void SpeechComand_Delete_m00454344C49B26A23A243433EB7136E54A5540E3 (void);
// 0x0000003B System.Void SpeechComand::ShowPart(System.String)
extern void SpeechComand_ShowPart_m65FB246220A09312A09D326F49DE1B2D2A055A40 (void);
// 0x0000003C System.Void SpeechComand::Stop()
extern void SpeechComand_Stop_m3118A509EF82A269482AFFF5316EBE0BB580D1FD (void);
// 0x0000003D System.Void SpeechComand::.ctor()
extern void SpeechComand__ctor_mADF99164A58684520B9CFC02F3BDFFEE1CA86A0A (void);
// 0x0000003E System.Void trigger::Start()
extern void trigger_Start_mD7D0D397DBC6CE802A454F4E107EBD84C5630981 (void);
// 0x0000003F System.Void trigger::OnTriggerEnter(UnityEngine.Collider)
extern void trigger_OnTriggerEnter_m5E2CEAE1AA4C358936C7371D06EDCA2D19B6C31A (void);
// 0x00000040 System.Void trigger::Update()
extern void trigger_Update_mB139430EF7366477F898399A6C321F219D0B64C6 (void);
// 0x00000041 System.Void trigger::.ctor()
extern void trigger__ctor_m8541964BCD27997F5B236A76CC8A59598E251F94 (void);
// 0x00000042 System.Void InformationLoader::LoadInformationAnatomieIliaque()
extern void InformationLoader_LoadInformationAnatomieIliaque_mA17BF5EB9F0672DC34E706E88B309FCA5497893E (void);
// 0x00000043 System.Void InformationLoader::UnloadInformationAnatomieIliaque()
extern void InformationLoader_UnloadInformationAnatomieIliaque_m663DFD136549A030AA33D0AA437C555B8933768A (void);
// 0x00000044 System.Void InformationLoader::LoadInformationOrganFemur()
extern void InformationLoader_LoadInformationOrganFemur_mBD06823AE4A84CD9E586F76AE7DF70DD7BF59574 (void);
// 0x00000045 System.Void InformationLoader::UnloadInformationOrganFemur()
extern void InformationLoader_UnloadInformationOrganFemur_m368111595A31E9EAFFA9A2D98A73CE0395516BD2 (void);
// 0x00000046 System.Void InformationLoader::.ctor()
extern void InformationLoader__ctor_m1A725C2C088B34734B597DEA4F4772D9128FB5D6 (void);
// 0x00000047 System.Void ModelsLoader::LoadAnatomieIliaque()
extern void ModelsLoader_LoadAnatomieIliaque_m14714E13D7C45633D33508484F604E2B862FD594 (void);
// 0x00000048 System.Void ModelsLoader::UnloadAnatomieIliaque()
extern void ModelsLoader_UnloadAnatomieIliaque_mFE4ED1B2E8083D50DB39FE458D460573DC8E2C50 (void);
// 0x00000049 System.Void ModelsLoader::LoadArtere()
extern void ModelsLoader_LoadArtere_mCFBDE91C822747EDA840E8EDE11E7CDBACCB0616 (void);
// 0x0000004A System.Void ModelsLoader::UnloadArtere()
extern void ModelsLoader_UnloadArtere_m874F3A28FE4604A38DEE62CA68F5085C2D1FC868 (void);
// 0x0000004B System.Void ModelsLoader::LoadIliaque()
extern void ModelsLoader_LoadIliaque_m2527D07F8B619B044E3BE3D035844FD4103195E0 (void);
// 0x0000004C System.Void ModelsLoader::UnloadIliaque()
extern void ModelsLoader_UnloadIliaque_m880D1130F06BC665DCD24A12174ADE12D303D58D (void);
// 0x0000004D System.Void ModelsLoader::LoadSacrum()
extern void ModelsLoader_LoadSacrum_m26AE42F43D1FC3EBE82FCFC0C5C892A098DC0093 (void);
// 0x0000004E System.Void ModelsLoader::UnloadSacrum()
extern void ModelsLoader_UnloadSacrum_m2009A0FBA6031F40EE55E6830D3E940551C73A3E (void);
// 0x0000004F System.Void ModelsLoader::LoadTumeur()
extern void ModelsLoader_LoadTumeur_mD30CD75DEDE78C6968DC232A4EE14B7E1D50EDE7 (void);
// 0x00000050 System.Void ModelsLoader::UnloadTumeur()
extern void ModelsLoader_UnloadTumeur_mBC1B7681108B0BCCCE1F5882B23F40CD40ED8EC2 (void);
// 0x00000051 System.Void ModelsLoader::LoadVeine()
extern void ModelsLoader_LoadVeine_m14ECA585C6BAAC83404EC43DF4F691E17D4731F1 (void);
// 0x00000052 System.Void ModelsLoader::UnloadVeine()
extern void ModelsLoader_UnloadVeine_m0403B53DD58E227FC3408F30253F2ECC954F3BB4 (void);
// 0x00000053 System.Void ModelsLoader::LoadGuideIliaque()
extern void ModelsLoader_LoadGuideIliaque_m94EFA1D6B1C4B56152699158A936425F811B80CF (void);
// 0x00000054 System.Void ModelsLoader::UnloadGuideIliaque()
extern void ModelsLoader_UnloadGuideIliaque_m151314573D32D8A39E4DC1F784138CC7EB364669 (void);
// 0x00000055 System.Void ModelsLoader::LoadOrganFemur()
extern void ModelsLoader_LoadOrganFemur_m3C10CBEB81655D31E30E67B9FE5F1C86881A880D (void);
// 0x00000056 System.Void ModelsLoader::UnloadOrganFemur()
extern void ModelsLoader_UnloadOrganFemur_mCFD7CF210E100D0C6A0A8C0EE7716DAF199B7DD1 (void);
// 0x00000057 System.Void ModelsLoader::LoadTumeurFemur()
extern void ModelsLoader_LoadTumeurFemur_m478DA2342919158952186604593909834CFE32D9 (void);
// 0x00000058 System.Void ModelsLoader::UnloadTumeurFemur()
extern void ModelsLoader_UnloadTumeurFemur_mB008C6379AFCC7641BC4A67D51166F5193F5A3B3 (void);
// 0x00000059 System.Void ModelsLoader::LoadFemur()
extern void ModelsLoader_LoadFemur_m768DCEFEFFC8671FF576395541B0F8A25603B07F (void);
// 0x0000005A System.Void ModelsLoader::UnloadFemur()
extern void ModelsLoader_UnloadFemur_m21D0ED30F55A3BD0819A53E3545DC9DB16A6A3BC (void);
// 0x0000005B System.Void ModelsLoader::LoadGuideFemur()
extern void ModelsLoader_LoadGuideFemur_m68DF81714258933D19A537A72951BA6E277004BD (void);
// 0x0000005C System.Void ModelsLoader::UnloadGuideFemur()
extern void ModelsLoader_UnloadGuideFemur_mB464E22DF159667104B988955E3D29CBCD518146 (void);
// 0x0000005D System.Void ModelsLoader::.ctor()
extern void ModelsLoader__ctor_m4388E67B02F88D24FC4A6422E98EA6686E4F15A7 (void);
// 0x0000005E System.Void SceneLoader::LoadMainScene()
extern void SceneLoader_LoadMainScene_m94B050FBEBF905BE839DA6570A04E7233E00596A (void);
// 0x0000005F System.Void SceneLoader::LoadTutorial()
extern void SceneLoader_LoadTutorial_mF01A3913D97D5B5DD75CB0C5E97C8A7492A59A22 (void);
// 0x00000060 System.Void SceneLoader::LoadOperationScene()
extern void SceneLoader_LoadOperationScene_mD69B6E930B626DFC206CDA65CBBDFDBBD8DE7D9F (void);
// 0x00000061 System.Void SceneLoader::Load3DModels()
extern void SceneLoader_Load3DModels_mB59923420D5A4616E323F262F875F1C2489B1BEF (void);
// 0x00000062 System.Void SceneLoader::Quit()
extern void SceneLoader_Quit_mF32E5EF6293904BE636AFD155BA71D5B35BDD576 (void);
// 0x00000063 System.Void SceneLoader::LoadParameters()
extern void SceneLoader_LoadParameters_m9CFF852FE588622748A0C4C9907ABF1A31526F82 (void);
// 0x00000064 System.Void SceneLoader::UnloadParameters()
extern void SceneLoader_UnloadParameters_m8BBD95DC74D47CF07005D49F765E1E1D2484220A (void);
// 0x00000065 System.Void SceneLoader::LoadAnatomieIliaque()
extern void SceneLoader_LoadAnatomieIliaque_m5E6634C69616462FD5317DE51A09614309A086A5 (void);
// 0x00000066 System.Void SceneLoader::UnloadAnatomieIliaque()
extern void SceneLoader_UnloadAnatomieIliaque_m2DF7C4BBAB7E1B24E539E24BB4E9786AA0865862 (void);
// 0x00000067 System.Void SceneLoader::LoadOrganFemur()
extern void SceneLoader_LoadOrganFemur_m8F608F1F0356D460B845D0131F2CA1EEAF21986B (void);
// 0x00000068 System.Void SceneLoader::UnloadOrganFemur()
extern void SceneLoader_UnloadOrganFemur_m569430AF689824A869E3394030CD3C684CF17E80 (void);
// 0x00000069 System.Void SceneLoader::.ctor()
extern void SceneLoader__ctor_m2248766DF38AF07562AD31501C7275B8DF1B7D29 (void);
// 0x0000006A System.Void UIManagerOperationScene::Start()
extern void UIManagerOperationScene_Start_mB64E5CA726DA3F2A242499F5FF3005223132791A (void);
// 0x0000006B System.Void UIManagerOperationScene::MainMenu()
extern void UIManagerOperationScene_MainMenu_mC342C7513E0EE415678606FD56C81219B40BCC5C (void);
// 0x0000006C System.Void UIManagerOperationScene::.ctor()
extern void UIManagerOperationScene__ctor_mD6CF427F3AC2973B4F113B574AD75DC1D3FDADBB (void);
static Il2CppMethodPointer s_methodPointers[108] = 
{
	DisableObjectAfterDelay_Awake_m257142B35642921833E778F36587AA3192378465,
	DisableObjectAfterDelay_DisableAfterDelay_m5B9F12CEF60C3495D2A1465F800C269A35AF7E2E,
	DisableObjectAfterDelay__ctor_m276DE3357BEE86BC1C3FC21D023F0BFCF150E127,
	U3CDisableAfterDelayU3Ed__4__ctor_m2C91CE34B94743AF7EF50E9214C9AEA7FCA79515,
	U3CDisableAfterDelayU3Ed__4_System_IDisposable_Dispose_mA39E91D4A0D4AFFEAA7E41B928EC362268D50DD1,
	U3CDisableAfterDelayU3Ed__4_MoveNext_m8C18702750633BB81526459A569616D16E4910C6,
	U3CDisableAfterDelayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m25813633BAE9E500DB07C70EC0EE5DDF835F8CBD,
	U3CDisableAfterDelayU3Ed__4_System_Collections_IEnumerator_Reset_m2B139A7C8C9D16F95C1B3885A166600AE585A184,
	U3CDisableAfterDelayU3Ed__4_System_Collections_IEnumerator_get_Current_m0476275292D9774988C3B9B7F9C57B5082CBCE81,
	MenuManager_TrainingMode_m40DC838AC258FBDA58D6CDE823DA957DFA3EC017,
	MenuManager_TutorialMode_mA48FDDA254090470A8393FB52C56FED712D07CD9,
	MenuManager_OperationMode_mF6567B128251132E3BC86119AAD5952A7A9095BD,
	MenuManager__ctor_m07A22DFDD90E3164393F8BDE06DAEF5AFA786CF2,
	OrganButtonManager_ShowIliacSeperatlyButtonDown_mE86B8D0ABF2645713A2314B514C0E92AAC0CAB95,
	OrganButtonManager_ShowFemurSeperatlyButtonDown_m9E981F2769090F70833793E0847712DDB0179EDE,
	OrganButtonManager_LoadMeanMenu_mC7D6BD63CCE37B50705F6FAFDE453DFA967F324A,
	OrganButtonManager_ShowIliacButtonDown_m3BDEA4D8230A2A359B5DA0DB348AD74603226D66,
	OrganButtonManager_ShowFemurButtonDown_mBB95E8D881FDA5DF48006C84FAFB93690755203F,
	OrganButtonManager_ShowIliac_mD01D3F5A08B38CD571DA931864A723C94B44FD3A,
	OrganButtonManager_ShowFemur_m56CF9FC75C79C466937C2A475E2232DF4D60CCCD,
	OrganButtonManager_Back_m3463414A603340BDD3EBE6B8C32CB51D10F69625,
	OrganButtonManager__ctor_mA4AF11F58F85BE830FC0252F885C9C660EE2820E,
	TutoText_Start_m45D41A03F949D48A0BD4DC2B60B4422A7947294E,
	TutoText_changeText_m0A4ABE4A295BE6F936EB3D1D0FC9DD2250E9F01B,
	TutoText__ctor_m412B2AA162971446A7F8AFB6AE4A796D4D5BF19D,
	U3CchangeTextU3Ed__4__ctor_mB330C1623ECE7F111B177AE55030DF1D77681463,
	U3CchangeTextU3Ed__4_System_IDisposable_Dispose_m5A7DFD8A15AF50E78CC0E93E96A1E6693477C625,
	U3CchangeTextU3Ed__4_MoveNext_m611360DE62ECB9BB15198A4D7D684911FFA4AE8F,
	U3CchangeTextU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB1F7B9FE3E8378FDB8C74A19C96901C075BB839,
	U3CchangeTextU3Ed__4_System_Collections_IEnumerator_Reset_mFCD0DF78872A7A730C40E2B26578444106376932,
	U3CchangeTextU3Ed__4_System_Collections_IEnumerator_get_Current_mCCB3E244B009BBC0D66F9A3483B50D48756BDDC5,
	UIManagerTrainingMode_Awake_m71DC8B18A515BC08A1940A448BB5719F108E65AB,
	UIManagerTrainingMode_Update_m320C0460DA1DCB187C3761CF3425F2B36ECC26BB,
	UIManagerTrainingMode_RestartTuto_m94F9FB57EEA717669862B7A9F5C007E6CB8FB2EF,
	UIManagerTrainingMode_MainMenu_mA0435FE7D0084E345EB05E2D1CE3F07C1DC18E06,
	UIManagerTrainingMode__ctor_mAB12FDB875F94B67FAE267516B441339E35F9E9C,
	controller_Start_mB26A4CABF956423246728470990213D287A89728,
	controller_Update_mDC1CCBA134CA67C1ABF0FD50B477BE2F339A6E8D,
	controller__ctor_mCC811B140CF89AADD1C0FA58475114264BBF8379,
	cutPaint_Start_m0E8F15BFEEDA8B963D865768A5BB425C579AF7AE,
	cutPaint_Update_m35CDDD6CF29225D962C0CC62DA76A23FC9733571,
	cutPaint__ctor_m7DB70F72753ADA43A7389F1AFA7147385FB5EF67,
	imageTrack_Start_mD6AFC5AB0198D054A984660C9EB04F3BE52CBEDB,
	imageTrack_Update_m7D39085929333C0865293CF97C336CAC8E4198FE,
	imageTrack__ctor_m077A4E089D7B3B498C8E0C0FC850E452077578F4,
	NewCutPaint_Start_m3432265C417B3002CED3696966E26383F90844C1,
	NewCutPaint_Update_mAD2308D19F74958943525D1BEC6EBE008C8FD649,
	NewCutPaint__ctor_m50326C6450CA38648BFF12E56DF163766A62D458,
	QRCodeManager_ActivateQR_m785C3E1B4D782DE9336928669D130F2BEAA6D98F,
	QRCodeManager_DisactivateQR_mDC70CACA14E2D7F1F3F02388298FAA08BFA2E04F,
	QRCodeManager__ctor_m098D357199051BDF3C730F2221292CFE399AF4C1,
	SpeechComand_Update_mFB38D6541D44393E55F84E3ECDA58EF0A23990FD,
	SpeechComand_SetPosition_mA1DBC7BD199ADB006070B1C90D77573A2D0FB670,
	SpeechComand_Show_m82009FD51C38D6262E57D4C853E401F23F97239A,
	SpeechComand_Zoom_m2CACFE71B1BBCCEDEF01841485CAEFF48000BE08,
	SpeechComand_Reset_mEE9A8603DA5B1D881594FCB3998CD222370F6695,
	SpeechComand_Destroy_mE2A3F0BA7671C0D24EC818742E84B2E198389A73,
	SpeechComand_Delete_m00454344C49B26A23A243433EB7136E54A5540E3,
	SpeechComand_ShowPart_m65FB246220A09312A09D326F49DE1B2D2A055A40,
	SpeechComand_Stop_m3118A509EF82A269482AFFF5316EBE0BB580D1FD,
	SpeechComand__ctor_mADF99164A58684520B9CFC02F3BDFFEE1CA86A0A,
	trigger_Start_mD7D0D397DBC6CE802A454F4E107EBD84C5630981,
	trigger_OnTriggerEnter_m5E2CEAE1AA4C358936C7371D06EDCA2D19B6C31A,
	trigger_Update_mB139430EF7366477F898399A6C321F219D0B64C6,
	trigger__ctor_m8541964BCD27997F5B236A76CC8A59598E251F94,
	InformationLoader_LoadInformationAnatomieIliaque_mA17BF5EB9F0672DC34E706E88B309FCA5497893E,
	InformationLoader_UnloadInformationAnatomieIliaque_m663DFD136549A030AA33D0AA437C555B8933768A,
	InformationLoader_LoadInformationOrganFemur_mBD06823AE4A84CD9E586F76AE7DF70DD7BF59574,
	InformationLoader_UnloadInformationOrganFemur_m368111595A31E9EAFFA9A2D98A73CE0395516BD2,
	InformationLoader__ctor_m1A725C2C088B34734B597DEA4F4772D9128FB5D6,
	ModelsLoader_LoadAnatomieIliaque_m14714E13D7C45633D33508484F604E2B862FD594,
	ModelsLoader_UnloadAnatomieIliaque_mFE4ED1B2E8083D50DB39FE458D460573DC8E2C50,
	ModelsLoader_LoadArtere_mCFBDE91C822747EDA840E8EDE11E7CDBACCB0616,
	ModelsLoader_UnloadArtere_m874F3A28FE4604A38DEE62CA68F5085C2D1FC868,
	ModelsLoader_LoadIliaque_m2527D07F8B619B044E3BE3D035844FD4103195E0,
	ModelsLoader_UnloadIliaque_m880D1130F06BC665DCD24A12174ADE12D303D58D,
	ModelsLoader_LoadSacrum_m26AE42F43D1FC3EBE82FCFC0C5C892A098DC0093,
	ModelsLoader_UnloadSacrum_m2009A0FBA6031F40EE55E6830D3E940551C73A3E,
	ModelsLoader_LoadTumeur_mD30CD75DEDE78C6968DC232A4EE14B7E1D50EDE7,
	ModelsLoader_UnloadTumeur_mBC1B7681108B0BCCCE1F5882B23F40CD40ED8EC2,
	ModelsLoader_LoadVeine_m14ECA585C6BAAC83404EC43DF4F691E17D4731F1,
	ModelsLoader_UnloadVeine_m0403B53DD58E227FC3408F30253F2ECC954F3BB4,
	ModelsLoader_LoadGuideIliaque_m94EFA1D6B1C4B56152699158A936425F811B80CF,
	ModelsLoader_UnloadGuideIliaque_m151314573D32D8A39E4DC1F784138CC7EB364669,
	ModelsLoader_LoadOrganFemur_m3C10CBEB81655D31E30E67B9FE5F1C86881A880D,
	ModelsLoader_UnloadOrganFemur_mCFD7CF210E100D0C6A0A8C0EE7716DAF199B7DD1,
	ModelsLoader_LoadTumeurFemur_m478DA2342919158952186604593909834CFE32D9,
	ModelsLoader_UnloadTumeurFemur_mB008C6379AFCC7641BC4A67D51166F5193F5A3B3,
	ModelsLoader_LoadFemur_m768DCEFEFFC8671FF576395541B0F8A25603B07F,
	ModelsLoader_UnloadFemur_m21D0ED30F55A3BD0819A53E3545DC9DB16A6A3BC,
	ModelsLoader_LoadGuideFemur_m68DF81714258933D19A537A72951BA6E277004BD,
	ModelsLoader_UnloadGuideFemur_mB464E22DF159667104B988955E3D29CBCD518146,
	ModelsLoader__ctor_m4388E67B02F88D24FC4A6422E98EA6686E4F15A7,
	SceneLoader_LoadMainScene_m94B050FBEBF905BE839DA6570A04E7233E00596A,
	SceneLoader_LoadTutorial_mF01A3913D97D5B5DD75CB0C5E97C8A7492A59A22,
	SceneLoader_LoadOperationScene_mD69B6E930B626DFC206CDA65CBBDFDBBD8DE7D9F,
	SceneLoader_Load3DModels_mB59923420D5A4616E323F262F875F1C2489B1BEF,
	SceneLoader_Quit_mF32E5EF6293904BE636AFD155BA71D5B35BDD576,
	SceneLoader_LoadParameters_m9CFF852FE588622748A0C4C9907ABF1A31526F82,
	SceneLoader_UnloadParameters_m8BBD95DC74D47CF07005D49F765E1E1D2484220A,
	SceneLoader_LoadAnatomieIliaque_m5E6634C69616462FD5317DE51A09614309A086A5,
	SceneLoader_UnloadAnatomieIliaque_m2DF7C4BBAB7E1B24E539E24BB4E9786AA0865862,
	SceneLoader_LoadOrganFemur_m8F608F1F0356D460B845D0131F2CA1EEAF21986B,
	SceneLoader_UnloadOrganFemur_m569430AF689824A869E3394030CD3C684CF17E80,
	SceneLoader__ctor_m2248766DF38AF07562AD31501C7275B8DF1B7D29,
	UIManagerOperationScene_Start_mB64E5CA726DA3F2A242499F5FF3005223132791A,
	UIManagerOperationScene_MainMenu_mC342C7513E0EE415678606FD56C81219B40BCC5C,
	UIManagerOperationScene__ctor_mD6CF427F3AC2973B4F113B574AD75DC1D3FDADBB,
};
static const int32_t s_InvokerIndices[108] = 
{
	8868,
	8713,
	8868,
	6975,
	8868,
	8570,
	8713,
	8868,
	8713,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8713,
	8868,
	6975,
	8868,
	8570,
	8713,
	8868,
	8713,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	7022,
	8868,
	8868,
	7022,
	7022,
	7022,
	8868,
	8868,
	8868,
	7022,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
	8868,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	108,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
