using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
   public void TrainingMode()
    {
        SceneManager.LoadScene("TrainingScene");
    }
    public void TutorialMode()
    {
        SceneManager.LoadScene("TutorialScene");
    }
    public void OperationMode()
    {
        SceneManager.LoadScene("OperationScene");
    }
}
