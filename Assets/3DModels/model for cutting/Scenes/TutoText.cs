using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TutoText : MonoBehaviour
{
    public TextMeshPro m_TutoText;
    public string[] m_Text;
    public bool finish=false;
    // Start is called before the first frame update
    void Start()
    {
        finish= false;
        StartCoroutine(changeText());
    }


    IEnumerator changeText() 
    {
        foreach(string s in m_Text)
        {
            m_TutoText.text = s;    
            yield return new WaitForSeconds(3);
        }
        finish= true;
        
       
    }

}
