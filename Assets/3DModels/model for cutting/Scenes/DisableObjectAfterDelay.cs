using UnityEngine;
using System.Collections;

public class DisableObjectAfterDelay : MonoBehaviour
{
    public GameObject MeanMenu;
    public GameObject LogoMenu;
    public bool isOpen = true;
    void Awake()
    {
        if(isOpen)
        {
            LogoMenu.SetActive(true);
            MeanMenu.SetActive(false);
            StartCoroutine(DisableAfterDelay());
        }
        else
        {
            LogoMenu.SetActive(false);
            MeanMenu.SetActive(true);
        }
        isOpen = false;

    }

    IEnumerator DisableAfterDelay()
    {
        yield return new WaitForSeconds(1);
        LogoMenu.SetActive(false);
        MeanMenu.SetActive(true);
        
    }
}
