using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OrganButtonManager : MonoBehaviour
{
    [Header("Organ Canvas Tutorial")]
    public GameObject OrganCanvasIliac;
    public GameObject OrganCanvasFemur;
    [Header("OrganMeanMenu")]
    public GameObject MeanTuto;
    
    [Header("OrganTutorial")]
    public GameObject iliacTuto;
    public GameObject fumeurTuto;
    [Header("OrganSeparate")]
    public GameObject iliacSep;
    public GameObject fumeurSep;

    
    public void ShowIliacSeperatlyButtonDown()
    {
        iliacSep.SetActive(true);
        iliacTuto.SetActive(false);
    }
    public void ShowFemurSeperatlyButtonDown()
    {
        fumeurSep.SetActive(true);
        fumeurTuto.SetActive(false);
    }

    public void LoadMeanMenu()
    {
        SceneManager.LoadScene("MeanScene");
    }

    public void ShowIliacButtonDown()
    {
        iliacSep.SetActive(false);
        iliacTuto.SetActive(true);
    }

    public void ShowFemurButtonDown()
    {
        fumeurSep.SetActive(false);
        fumeurTuto.SetActive(true);
    }

    public void ShowIliac()
    {
        iliacTuto.SetActive(true);
        iliacSep.SetActive(false);
        MeanTuto.SetActive(false);
        fumeurSep.SetActive(false);
        fumeurTuto.SetActive(false);
        OrganCanvasIliac.SetActive(true);
        OrganCanvasFemur.SetActive(false);
    }
    public void ShowFemur()
    {
        iliacTuto.SetActive(false);
        iliacSep.SetActive(false);
        MeanTuto.SetActive(false);
        fumeurSep.SetActive(false);
        fumeurTuto.SetActive(true);
        OrganCanvasIliac.SetActive(false);
        OrganCanvasFemur.SetActive(true);
        
    }

    public void Back()
    {
        iliacSep.SetActive(false);
        MeanTuto.SetActive(true);
        fumeurSep.SetActive(false);
        fumeurTuto.SetActive(false);
        iliacTuto.SetActive(false);

        OrganCanvasIliac.SetActive(false) ;
        OrganCanvasFemur.SetActive(false);



    }

}
