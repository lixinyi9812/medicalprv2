using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManagerTrainingMode : MonoBehaviour
{
    public bool finish;
    public GameObject UIFinish;
    public GameObject TextUI;

    public TutoText Tuto_text;
    // Start is called before the first frame update
    void Awake()
    {
        finish = Tuto_text.finish;
        TextUI.SetActive(true);
        UIFinish.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        finish = Tuto_text.finish;
        if (finish)
        {
            TextUI.SetActive(false);
            UIFinish.SetActive(true);
        }
    }
     
    public void RestartTuto()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MeanScene");
    }
}
