using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public GameObject parameters;
    public GameObject modelsIliaque;
    public GameObject modelsFemur;
    // Start is called before the first frame update
    public void LoadMainScene()
    {
        SceneManager.LoadScene("mainScene");
    }
    public void LoadTutorial()
    {
        SceneManager.LoadScene("Tutorial");
    }
    public void LoadOperationScene()
    {
        SceneManager.LoadScene("OperationScene");
    }
    public void Load3DModels()
    {
        SceneManager.LoadScene("3DModels");
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void LoadParameters()
    {
        parameters.SetActive(true);
    }
    public void UnloadParameters()
    {
        parameters.SetActive(false);
    }
    public void LoadAnatomieIliaque()
    {
        modelsIliaque.SetActive(true);
    }
    public void UnloadAnatomieIliaque()
    {
        modelsIliaque.SetActive(false);
    }
    public void LoadOrganFemur()
    {
        modelsFemur.SetActive(true);
    }
    public void UnloadOrganFemur()
    {
        modelsFemur.SetActive(false);
    }
}
