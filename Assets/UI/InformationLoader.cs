using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InformationLoader : MonoBehaviour
{
    public GameObject information;
    // Start is called before the first frame update
    public void LoadInformationAnatomieIliaque()
    {
        information.SetActive(true);
        information.transform.Find("Text").gameObject.GetComponent<TMP_Text>().text = "Information about the model 'Anatomie iliaque', its particularities, etc...";
    }
    public void UnloadInformationAnatomieIliaque()
    {
        information.SetActive(false);
        information.transform.Find("Text").gameObject.GetComponent<TextMeshPro>().text = " ";
    }
    public void LoadInformationOrganFemur()
    {
        information.SetActive(true);
        information.transform.Find("Text").gameObject.GetComponent<TextMeshPro>().text = "Information about the model 'OrganFemur', its particularities, etc...";
    }
    public void UnloadInformationOrganFemur()
    {
        information.SetActive(false);
        information.transform.Find("Text").gameObject.GetComponent<TextMeshPro>().text = " ";
    }

}
