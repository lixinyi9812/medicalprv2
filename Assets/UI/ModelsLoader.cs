using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelsLoader : MonoBehaviour
{
    public GameObject iliaque;
    public GameObject femur;


    // Start is called before the first frame update
    public void LoadAnatomieIliaque()
    {
        iliaque.SetActive(true);
    }
    public void UnloadAnatomieIliaque()
    {
        iliaque.SetActive(false);
    }
    public void LoadArtere()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Art�re").gameObject.SetActive(true);
    }
    public void UnloadArtere()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Art�re").gameObject.SetActive(false);
    }
    public void LoadIliaque()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Iliaque").gameObject.SetActive(true);
    }
    public void UnloadIliaque()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Iliaque").gameObject.SetActive(false);
    }
    public void LoadSacrum()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Sacrum").gameObject.SetActive(true);
    }
    public void UnloadSacrum()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Sacrum").gameObject.SetActive(false);
    }
    public void LoadTumeur()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Tumeur").gameObject.SetActive(true);
    }
    public void UnloadTumeur()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Tumeur").gameObject.SetActive(false);
    }
    public void LoadVeine()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Veine").gameObject.SetActive(true);
    }
    public void UnloadVeine()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Veine").gameObject.SetActive(false);
    }
    public void LoadGuideIliaque()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Guide de coupe").gameObject.SetActive(true);
    }
    public void UnloadGuideIliaque()
    {
        iliaque.transform.Find("Pivot").gameObject.transform.Find("Guide de coupe").gameObject.SetActive(false);
    }
    public void LoadOrganFemur()
    {
        femur.SetActive(true);
    }
    public void UnloadOrganFemur()
    {
        femur.SetActive(false);
    }
    public void LoadTumeurFemur()
    {
        femur.transform.Find("Tumor").gameObject.SetActive(true);
    }
    public void UnloadTumeurFemur()
    {
        femur.transform.Find("Tumor").gameObject.SetActive(false);
    }
    public void LoadFemur()
    {
        femur.transform.Find("F�mur").gameObject.SetActive(true);
    }
    public void UnloadFemur()
    {
        femur.transform.Find("F�mur").gameObject.SetActive(false);
    }
    public void LoadGuideFemur()
    {
        femur.transform.Find("CuttingGuide").gameObject.SetActive(true);
    }
    public void UnloadGuideFemur()
    {
        femur.transform.Find("CuttingGuide").gameObject.SetActive(false);
    }
}
