using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trigger : MonoBehaviour
{
    public GameObject marker;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Organ")
        {
            Debug.Log("gg");
            Vector3 hitPos = other.bounds.ClosestPoint(transform.position);
            GameObject sphere = Instantiate(marker, hitPos, Quaternion.identity);
        }
    }


    void Update()
    {
        
    }
}
