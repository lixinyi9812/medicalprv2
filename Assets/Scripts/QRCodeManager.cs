using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QRCodeManager : MonoBehaviour
{
    public bool QRmode = false;
   public void ActivateQR()
    {
        QRmode = true;
        Transform[] QRCodes = GetComponentsInChildren<Transform>(true);
        foreach (Transform qr in QRCodes)
        {
            qr.gameObject.SetActive(true);
        }

    }

    public void DisactivateQR()
    {
        QRmode = false;
        Transform[] QRCodes = GetComponentsInChildren<Transform>(true);
        foreach (Transform qr in QRCodes)
        {
            qr.gameObject.SetActive(false);
        }
    }

}
