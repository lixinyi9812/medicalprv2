using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;

public class cutPaint : MonoBehaviour
{
    //private Texture2D paintTex;
    //private int r = 50;

    //public GameObject sphereMarker;
    GameObject thumbObject;
    MixedRealityPose pose;
    IMixedRealityPointer pointer;
    Ray ray;
    RaycastHit hit;
    LineRenderer lineRenderer;
    List<Vector3> hitpoints;
    public GameObject marker;
    public Collider ObjectCollider;
    public GameObject MRplayspace;
    bool pressed;
    // Start is called before the first frame update
    void Start()
    {


        ray = new Ray(this.transform.position, Camera.main.transform.forward);
        lineRenderer = GetComponent<LineRenderer>();
        hitpoints = new List<Vector3>();
    }




    // Update is called once per frame
    void Update()
    {
        if (HandJointUtils.TryGetJointPose(TrackedHandJoint.ThumbTip, Handedness.Right, out pose))
        {

            if (MRplayspace.transform.Find("Right_ShellHandRayPointer(Clone)"))
            {
                pressed = MRplayspace.transform.Find("Right_ShellHandRayPointer(Clone)").GetComponent<ShellHandRayPointer>().isPressed;
                Debug.Log("pressed=");
                Debug.Log(pressed);
            }
            else
            {
                Debug.Log("cant find shellhand ray");
            }

            if (pressed)
            {
                ray.origin = pose.Position;
                //if (Physics.Raycast(ray, out hit))
                //{
                    if (hit.collider == ObjectCollider)
                    {
                        hitpoints.Add(hit.point);
                        GameObject sphere = Instantiate(marker, hit.point, Quaternion.identity);

                    }

                //}

                
            }

            lineRenderer.SetPosition(0, pose.Position);
            lineRenderer.SetPosition(1, pose.Position + Camera.main.transform.forward);
            lineRenderer.widthMultiplier = 0.01f;


        }

    }
}
