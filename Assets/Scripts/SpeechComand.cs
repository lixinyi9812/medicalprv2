using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechComand : MonoBehaviour
{
  

    [Header("Models Manipulation")]
    public Vector3 scaleChange;
    public Vector3 QrcodePosition;
    public DefaultObserverEventHandler poCompo;
    public GameObject imageTarget;
    

    public  void Update()
    {
        QrcodePosition = poCompo.pos;
    }
    public void SetPosition()
    {
        QrcodePosition = poCompo.pos;
    }
    public void Show(GameObject obj)
    {
        
        if (obj != null)
        {
            Instantiate(obj, transform.position + 0.5f * transform.forward, Quaternion.identity);

        }

    }

    
    public void Zoom()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Organ");
        foreach (GameObject go in objs)
        {
            go.transform.localScale += scaleChange;
        }     
    }
    public void Reset()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Organ");
        foreach (GameObject go in objs)
        {
            go.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public  void Destroy(string organ)
    {
        GameObject org = GameObject.Find(organ);
        Destroy(organ);
    }


    public void Delete(string partOrgan)
    {
        GameObject org = GameObject.FindGameObjectWithTag("Organ");
        GameObject[] partOrg = GetComponentsInChildren<GameObject>(true);

        foreach (GameObject qr in partOrg)
        {
            if (qr.name == "Pivot")
            {
                
                GameObject[] org1 = qr.GetComponentsInChildren<GameObject>(true);
                foreach (GameObject go in org1)
                {
                    if (go.name == partOrgan)
                    {
                        go.SetActive(false);
                        return;
                    }
                }
                return;
            }
            else if (qr.name == partOrgan)
            {
                qr.SetActive(false);
            }
        }
    }
    public void ShowPart(string partOrgan)
    {

        GameObject org = GameObject.FindGameObjectWithTag("Organ");
        GameObject[] partOrg = GetComponentsInChildren<GameObject>(true);

        foreach (GameObject qr in partOrg)
        {
            
            if (qr.name == "Pivot")
            {
                GameObject[] org1 = qr.GetComponentsInChildren<GameObject>(true);
                foreach(GameObject go in org1)
                {
                    if (go.name == partOrgan)
                    {
                        go.SetActive(true);
                        return;
                    }
                }
                return;
            }
            else if (qr.name == partOrgan)
            {
                qr.SetActive(true);
            }
            
        }
       

    }
    public  void Stop()
    {
        GameObject[] organs = GameObject.FindGameObjectsWithTag("Organ");
        if (organs!=null&& organs.Length>0)
        {
            foreach (GameObject org in organs)
            {
                Destroy(org);
            }
        }
        
    }


   
}
