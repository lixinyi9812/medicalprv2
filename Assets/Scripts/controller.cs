using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Microsoft;
using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit.Input;

public class controller : MonoBehaviour
{
    public GameObject sphereMarker;
    GameObject thumbObject;
    MixedRealityPose pose;
    IMixedRealityPointer pointer;
    Ray ray;
    RaycastHit hit;
    Vector3 hitpoint;
    LineRenderer lineRenderer;
    List<Vector3> hitpoints;
    // Start is called before the first frame update
    void Start()
    {
       /* thumbObject = Instantiate(sphereMarker, this.transform);*/
        ray = new Ray(this.transform.position, this.transform.forward);
       lineRenderer = GetComponent<LineRenderer>();
    }


    // Update is called once per frame
    void Update()
    {

        if (HandJointUtils.TryGetJointPose(TrackedHandJoint.ThumbTip, Handedness.Right, out pose))
        {
            ray.origin = pose.Position;
            if(Physics.Raycast(ray, out hit))
            {
                hitpoint = hit.point;
                hitpoints.Add(hit.point);
                Debug.Log(hitpoint);
            }
            Debug.DrawRay(pose.Position, pose.Forward, Color.red);
            lineRenderer.SetPosition(0, pose.Position);
            lineRenderer.SetPosition(1, pose.Position+pose.Forward);
            lineRenderer.widthMultiplier = 0.01f;

        }

    }
}
